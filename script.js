let elementsToDisplay = ["hello", [1, 1, 12, 2], "world", "Kiev", ["London", "Mexico"], "New York",
    "Odessa", "Las Vegas",];
let elementToAttachTo = document.body;

let listElements = function (array, parent){
    let innerUl = document.createElement("ul");
    parent.prepend(innerUl);
    let newArray = array.map(function returnElements (item) {
        let element = `<li>${item}</li>`;
        if (Array.isArray(item)){
            returnElements();
        }
        return element;
    });
    innerUl.insertAdjacentHTML("afterbegin", newArray.join(""));
};

listElements(elementsToDisplay,elementToAttachTo);

setInterval(function (){
    elementToAttachTo.remove()
}, 3000);
